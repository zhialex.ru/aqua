import {Command} from "./command.class";
import {Telegraf} from "telegraf";

export class StartCommand extends Command {

    constructor(bot: Telegraf) {
        super(bot)
    }

    handle(): void {
        this.bot.start((ctx) => {
            const chat: Chat = ctx.message.chat
            const name = chat.username !== undefined ? chat.username : chat.first_name
            ctx.reply(`Hi, ${name}! If you want me to send you notifications when new episodes are released, please send me a link to the anime`)
        })
    }

}

type Chat = {
    id: number
    first_name?: string
    username?: string
    type?: string
}