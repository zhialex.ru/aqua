import {Command} from "./command.class";
import {Context, Telegraf} from "telegraf";
import {Deunionize} from "telegraf/typings/deunionize";

export class AddAnimeCommand extends Command {

    url_trigger: RegExp =  new RegExp("https://smotret-anime.com/catalog/\S*")

    constructor(bot: Telegraf) {
        super(bot)
    }


    handle(): void {
        this.bot.url(this.url_trigger, (ctx: Context<Deunionize>) => {

            console.log(ctx.)
        })
    }
}