import {IConfigService} from './config.interface';
import {config, DotenvParseOutput} from 'dotenv'

export class ConfigService implements IConfigService {

    private readonly config: DotenvParseOutput

    constructor() {
        const {error, parsed} = config()
        if (error) throw new Error('.env not found')
        if (!parsed) throw new Error('.env is empty')
        this.config = parsed
    }

    get(key: string): string {
        const res = this.config[key]
        if (!res) throw new Error(`can't find key: ${key}`)
        return res
    }
}
