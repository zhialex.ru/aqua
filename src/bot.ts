import {Telegraf} from "telegraf";
import {Command} from "./commands/command.class";
import {IConfigService} from "./config/config.interface";
import {StartCommand} from "./commands/start.command";
import {AddAnimeCommand} from "./commands/add.anime";

export class Bot {

    bot: Telegraf
    commands: Command[] = []

    constructor(private readonly configService: IConfigService) {
        this.bot = new Telegraf(this.configService.get('TOKEN'))
    }

    init() {
        this.commands = [new StartCommand(this.bot), new AddAnimeCommand(this.bot)]
        for (const command of this.commands) command.handle()
        this.bot.launch()
    }
}