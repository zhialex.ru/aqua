import {ConfigService} from "./config/config.service";
import {Bot} from "./bot";

const bot = new Bot(new ConfigService())
bot.init()